# require(Hmisc)
# 最大最小值归一化算法
rm(list = ls())

max_min_normalization = function(arr){
  max_value = max(arr)
  min_value = min(arr)
  return((arr-min_value)/(max_value-min_value))
}


category_id = 3408
sh_weight = c(0.5, 0.5)
root_dir = "C:/Users/Administrator/Desktop/rec_app_v3_3/model"
setwd(paste0(root_dir, "/", category_id))

category_heat_file = paste0(category_id, "_heat.csv")
category_heat = read.csv(category_heat_file, header=FALSE)
colnames(category_heat) = c("product_id", "heat_score")

files = dir(path = "./csv", pattern = '*.csv')


pt_csv = files[70]
product_id = sub(".csv", "", pt_csv)
title1 = paste0("category_id:", category_id, " & product_id: ", product_id)
test = read.csv(paste0("./csv/", pt_csv), header=FALSE)
  
col_nums = ncol(test)
row_nums = nrow(test)
  
colnames(test) = c("product_id", "similarity", "hot_score")
test$hot_score[is.nan(test$hot_score)] = 0
  
dev.new()

x_data = seq_len(row_nums)
s_data = max_min_normalization(test$similarity)
h_data = max_min_normalization(test$hot_score)
  
z_data_1 = 0.5*s_data + 0.5*h_data
z_data_2 = 0.6*s_data + 0.4*h_data
z_data_3 = 0.4*s_data + 0.6*h_data
  
max_z = max(max(z_data_3), max(z_data_2), max(z_data_1))
  
ymax = max(max(s_data), max(h_data), max_z)
  
plot(x_data, s_data, type = "b", pch = 15, lty = 1, 
       col = "red", ylim = c(0, ymax), main = title1,
       xlab = "rec_pts",ylab = "score", cex = 0.5)
  
lines(x_data, h_data, type="b", pch=17, lty=2, col="blue")
  
lines(x_data, z_data_1, type="b", pch=21, lty=4, cex=0.5, lwd=0.5, col="cyan2")
lines(x_data, z_data_2, type="b", pch=22, lty=5, cex=0.5, lwd=0.5, col="green")
lines(x_data, z_data_3, type="b", pch=23, lty=6, cex=0.5, lwd=0.5, col="magenta")
  
legend("topright", inset = 0.05, title = "score type", 
         legend = c("similarity", "hot_score", "s-5 : h-5", 
                    "s-6 : h-4", "s-4 : h-6"), 
         lty = c(1, 2, 4, 5, 6),
         pch = c(15, 17, 21, 22, 23), 
         col = c("red", "blue", "cyan2", "green", "magenta"),
         cex = 0.5)
}
