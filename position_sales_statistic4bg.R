# Title     : 线上、离线推荐商品销售指标对比
# Created by: dinle
# Created on: 2017/10/18

require(RSQLite)
rm(list = ls())

root_path = "D:/dinle_work/data_analysis_module/data_analysis"
setwd(root_path)
database_path = "../tmp_data/analysis4bg_min64.db"
file_name = "rec_position_statistics_bg_min64_1k.pdf"

this_time = format(Sys.time(), format = "%Y%m%d_%H%M%S/")

save_path= paste0(root_path, "/analysis_outcome/", this_time)
dir.create(save_path)

sql_conn = dbConnect(RSQLite::SQLite(),dbname=database_path)

negative2zero = function(arr){
  arr[which(arr<0)] = 0
  return(arr)
}

# (2.1) 线上推荐商品的销售指标随推荐位的变化趋势
online_sale_indexes_sql = "
select nb.rec_position as rec_position,
avg(CASE nb.ip WHEN -1 THEN 0 ELSE nb.ip END) as ip,
avg(CASE nb.vpc WHEN -1 THEN 0 ELSE nb.vpc END) as vpc,
avg(CASE nb.pvs WHEN -1 THEN 0 ELSE nb.pvs END) as pvs,
avg(CASE nb.change_rate WHEN -1 THEN 0 ELSE nb.change_rate*100 END) as change_rate, 
avg(CASE nb.click_rate WHEN -1 THEN 0 ELSE nb.click_rate*100 END) as click_rate,
avg(CASE nb.cc_rate WHEN -1 THEN 0 ELSE nb.cc_rate*100 END) as cc_rate,
avg(CASE nb.baskets_rate WHEN -1 THEN 0 ELSE nb.baskets_rate*100 END) as baskets_rate,
avg(CASE nb.pay_number WHEN -1 THEN 0 ELSE nb.pay_number END) as pay_number,
avg(CASE nb.pay_amount WHEN -1 THEN 0 ELSE nb.pay_amount END) as pay_amount,
avg(CASE nb.bp_rate WHEN -1 THEN 0 ELSE nb.bp_rate*100 END) as bp_rate,
avg(CASE nb.stay_time WHEN -1 THEN 0 ELSE nb.stay_time END) as stay_time,
avg(CASE nb.avg_stay_time WHEN -1 THEN 0 ELSE nb.avg_stay_time END) as avg_stay_time,
avg(CASE nb.reviews WHEN -1 THEN 0 ELSE nb.reviews END) as reviews,
avg(CASE nb.wish WHEN -1 THEN 0 ELSE nb.wish END) as wish,
avg(CASE nb.pending_number WHEN -1 THEN 0 ELSE nb.pending_number END) as pending_number,
avg(CASE nb.pending_amount WHEN -1 THEN 0 ELSE nb.pending_amount END) as pending_amount,
avg(CASE nb.pending_rate WHEN -1 THEN 0 ELSE nb.pending_rate*100 END) as pending_rate,
avg(CASE nb.bounces WHEN -1 THEN 0 ELSE nb.bounces END) as bounces,
avg(CASE nb.bounce_rate WHEN -1 THEN 0 ELSE nb.bounce_rate*100 END) as bounce_rate,
avg(CASE nb.problem30 WHEN -1 THEN 0 ELSE nb.problem30 END) as problem30,
avg(CASE nb.problem60 WHEN -1 THEN 0 ELSE nb.problem60 END) as problem60,
avg(CASE nb.title_similarity WHEN -1 THEN 0 ELSE nb.title_similarity END) as title_similarity
from (
select rpo.rec_pts, rpo.rec_position, rpo.title_similarity,
sd.ip, sd.vpc, sd.pvs,
sd.change_rate, sd.click_rate, sd.cc_rate,
sd.baskets_rate, sd.bp_rate, 
sd.pay_number, sd.pay_amount,
sd.stay_time, sd.avg_stay_time,
sd.reviews, sd.wish,
sd.pending_number, sd.pending_amount, sd.pending_rate, 
sd.bounces, sd.bounce_rate,
sd.problem30, sd.problem60
from rec_pts_online4bg rpo
join sales_data4bg sd on UPPER(rpo.rec_pts) = UPPER(sd.product_id)
) nb
group by nb.rec_position
"
# join (select distinct primary_pt from rec_pts_online4bg limit 80) prid on rpo.primary_pt=prid.primary_pt
# (2.2) 离线推荐商品的销售指标随推荐位的变化趋势

offline_sale_indexes_sql = "
select nb.rec_position as rec_position,
avg(CASE nb.ip WHEN -1 THEN 0 ELSE nb.ip END) as ip,
avg(CASE nb.vpc WHEN -1 THEN 0 ELSE nb.vpc END) as vpc,
avg(CASE nb.pvs WHEN -1 THEN 0 ELSE nb.pvs END) as pvs,
avg(CASE nb.change_rate WHEN -1 THEN 0 ELSE nb.change_rate*100 END) as change_rate, 
avg(CASE nb.click_rate WHEN -1 THEN 0 ELSE nb.click_rate*100 END) as click_rate,
avg(CASE nb.cc_rate WHEN -1 THEN 0 ELSE nb.cc_rate*100 END) as cc_rate,
avg(CASE nb.baskets_rate WHEN -1 THEN 0 ELSE nb.baskets_rate*100 END) as baskets_rate,
avg(CASE nb.pay_number WHEN -1 THEN 0 ELSE nb.pay_number END) as pay_number,
avg(CASE nb.pay_amount WHEN -1 THEN 0 ELSE nb.pay_amount END) as pay_amount,
avg(CASE nb.bp_rate WHEN -1 THEN 0 ELSE nb.bp_rate*100 END) as bp_rate,
avg(CASE nb.stay_time WHEN -1 THEN 0 ELSE nb.stay_time END) as stay_time,
avg(CASE nb.avg_stay_time WHEN -1 THEN 0 ELSE nb.avg_stay_time END) as avg_stay_time,
avg(CASE nb.reviews WHEN -1 THEN 0 ELSE nb.reviews END) as reviews,
avg(CASE nb.wish WHEN -1 THEN 0 ELSE nb.wish END) as wish,
avg(CASE nb.pending_number WHEN -1 THEN 0 ELSE nb.pending_number END) as pending_number,
avg(CASE nb.pending_amount WHEN -1 THEN 0 ELSE nb.pending_amount END) as pending_amount,
avg(CASE nb.pending_rate WHEN -1 THEN 0 ELSE nb.pending_rate*100 END) as pending_rate,
avg(CASE nb.bounces WHEN -1 THEN 0 ELSE nb.bounces END) as bounces,
avg(CASE nb.bounce_rate WHEN -1 THEN 0 ELSE nb.bounce_rate*100 END) as bounce_rate,
avg(CASE nb.problem30 WHEN -1 THEN 0 ELSE nb.problem30 END) as problem30,
avg(CASE nb.problem60 WHEN -1 THEN 0 ELSE nb.problem60 END) as problem60,
avg(CASE nb.title_similarity WHEN -1 THEN 0 ELSE nb.title_similarity END) as title_similarity
from (
select rpf.rec_pts, rpf.rec_position, rpf.title_similarity,
sd.ip, sd.vpc, sd.pvs,
sd.change_rate, sd.click_rate, sd.cc_rate,
sd.baskets_rate, sd.bp_rate, 
sd.pay_number, sd.pay_amount,
sd.stay_time, sd.avg_stay_time,
sd.reviews, sd.wish,
sd.pending_number, sd.pending_amount, sd.pending_rate, 
sd.bounces, sd.bounce_rate,
sd.problem30, sd.problem60
from rec_pts_offline4bg rpf
join sales_data4bg sd on UPPER(rpf.rec_pts) = UPPER(sd.product_id)
) nb
group by nb.rec_position
"
#join (select distinct primary_pt from rec_pts_offline4bg limit 80) prid on rpf.primary_pt=prid.primary_pt
# 取数据&归一化处理
online_sale_sta = dbGetQuery(sql_conn, online_sale_indexes_sql)
offline_sale_sta = dbGetQuery(sql_conn, offline_sale_indexes_sql)

online_sale_sta[-1] = apply(online_sale_sta[-1], 2, negative2zero)
offline_sale_sta[-1] = apply(offline_sale_sta[-1], 2, negative2zero)

# 可视化
col_nums = ncol(online_sale_sta)
col_name = colnames(online_sale_sta)
row_nums_on = nrow(online_sale_sta)
row_nums_off = nrow(online_sale_sta)

xmax = min(max(row_nums_on), min(row_nums_off))
x_data = seq_len(xmax)

setwd(save_path)
pdf(file_name)
for (i in 2: col_nums){
  # dev.new()
  titlei = paste0("sale index <",col_name[i],">")
  a_data = online_sale_sta[, i][0: xmax]
  mean_a = mean(a_data)
  b_data = offline_sale_sta[, i][0: xmax]
  mean_b = mean(b_data)
  ymax = max(max(a_data), max(b_data))
  
  plot(x_data, a_data, type = "b", pch = 15, lty = 1, 
       col = "blue", ylim = c(0, ymax), main = titlei,
       xlab = "recommend position（1-24）",ylab = "true value", cex = 0.5)
  lines(x_data, b_data, type="b", pch=17, lty=2, col="red")
  
  abline(h = mean_a, lty=4, col="blue", lwd=3)
  abline(h = mean_b, lty=6, col="red", lwd=3)
  
  legend("topright", inset = 0.05, title = "illustrate", 
         legend = c("A-test", "B-test"), lty = c(1, 2),
         pch = c(15, 17), col = c("blue", "red"), cex = 0.5)
}
dev.off()
dbConnect(sql_conn)

write.csv(online_sale_sta, "online_sale_sta.csv", row.names = FALSE)
write.csv(offline_sale_sta, "offline_sale_sta.csv", row.names = FALSE)
